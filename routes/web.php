<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['XssSanitizer']], function () {


    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/field/edit', [\App\Http\Controllers\FieldController::class, 'edit'])->name('field.edit');
    Route::get('/field/store', [\App\Http\Controllers\FieldController::class, 'store'])->name('field.store');
    Route::put('/field/update', [\App\Http\Controllers\FieldController::class, 'update'])->name('field.update');
    Route::get('/field/show', [\App\Http\Controllers\FieldController::class, 'show'])->name('field.show');
    Route::get('/field/search', [\App\Http\Controllers\FieldController::class, 'search'])->name('field.search');


    Route::get('/users/show', [\App\Http\Controllers\UserController::class, 'show'])->name('users.show');
    Route::get('/users/webservice', [\App\Http\Controllers\UserController::class, 'webservice'])->name('users.webservice');

    Route::post('/metadata/store', [\App\Http\Controllers\MetadataController::class, 'store'])->name('metadata.store');
});
