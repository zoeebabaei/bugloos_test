<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->string('field_name');
            $table->string('field_type')->nullable();
            $table->string('row_name')->nullable();
            $table->string('row_width')->nullable();
            $table->tinyInteger('show_in_list')->default('1');
            $table->tinyInteger('show_in_search')->default('1');
            $table->tinyInteger('sort')->default('1');
           // $table->integer('webservice_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
};
