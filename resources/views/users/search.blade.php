@unless(count($search_list) == 0)

    <div class="card">
    <div class="card-body">
        <h5 class="card-title"></h5>

        <!-- Horizontal Form -->
        <form id="search_form_users">
            @csrf

            @foreach($search_list as $search_item)

                <div class="row sm-3">
                    <div class="col-sm-4">
                        <input class="form-control" placeholder="{{$search_item}}" name="search_{{$search_item}}">

                    </div>
                </div>

            @endforeach

            <div class="text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form><!-- End Horizontal Form -->

    </div>
</div>
@endunless
