<div class="card">
    <div class="card-body">
        <h5 class="card-title"></h5>

        <!-- Horizontal Form -->
        <form method="POST" action="{{route('field.update')}}">
            @csrf
            @foreach($all_fields as $all_field)
                @method('put')

            <div class="row sm-3">
                <div class="col-sm-2">{{$all_field->field_name}}</div>
                <div class="col-sm-1"> show in:</div>
                <div class="col-sm-1">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" checked id="show_list{{$all_field->id}}" name="show_list[{{$all_field->id}}]">
                        <label class="form-check-label" for="show_list{{$all_field->id}}">
                            list
                        </label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" checked id="sort{{$all_field->id}}" name="sort[{{$all_field->id}}]">
                        <label class="form-check-label" for="sort{{$all_field->id}}">
                            sort
                        </label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" checked id="show_search{{$all_field->id}}" name="show_search[{{$all_field->id}}]">
                        <label class="form-check-label" for="show_search{{$all_field->id}}">
                            search
                        </label>
                    </div>
                    </div>

                <div class="col-sm-6">

                    <div class="row mb-3">
                        <div class="col-sm-4">
                            <input type="text" value="{{$all_field->row_name}}" name="row_name[{{$all_field->id}}]" class="form-control" placeholder="row name in list">
                            <input type="hidden" name="id[{{$all_field->id}}]" value="{{$all_field->id}}">
                            <input type="hidden" name="field_name[{{$all_field->id}}]" value="{{$all_field->field_name}}">

                        </div>
                        <div class="col-sm-4">
                            <input type="text" value="{{$all_field->row_width}}" name="row_width[{{$all_field->id}}]" class="form-control" placeholder="row width">
                        </div>
                        <div class="col-sm-4">
                            <select class="form-select" aria-label="field type" name="field_type[{{$all_field->id}}]">
                                <option value="" selected>field type</option>
                                <option value="string">string</option>
                                <option value="integer">integer</option>
                                <option value="float">float</option>
                                <option value="date">date</option>
                                <option value="enum">enum</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
                @endforeach

            <div class="text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form><!-- End Horizontal Form -->

    </div>
</div>
