@extends('layout')
@section('content')
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Bordered Tabs Justified</h5>

                    <!-- Bordered Tabs Justified -->
                    <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
                        <li class="nav-item flex-fill" role="presentation">
                            <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="allfields" aria-selected="true">table fields</button>
                        </li>

                        <li class="nav-item flex-fill" role="presentation">
                            <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="metadata" aria-selected="false">metadata</button>
                        </li>
                    </ul>
                    <div class="tab-content pt-2" id="borderedTabJustifiedContent">
                        <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="allfields-tab">
                            @include('fields.edit')
                        </div>

                        <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="metadata-tab">
                            @include('fields.metadata')
                        </div>
                    </div><!-- End Bordered Tabs Justified -->

                </div>
            </div>
        </div>
    </div>
</section>
@endsection('content')
