<div class="card">
    <div class="card-body">
        <h5 class="card-title">Horizontal Form</h5>

        <!-- Horizontal Form -->
        <form class="row g-3" method="POST" action="{{route('metadata.store')}}">
            @csrf
            <div id="inputFormRow">
                <div class="row">
            <div class="col-md-4">
                <div class="form-floating">
                    <input type="text" name="metadata_name[]" class="form-control" placeholder="name">
                    <label>name</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input type="text" name="metadata_content[]" class="form-control" placeholder="content">
                    <label>content</label>
                </div>
            </div>
            <div class="col-md-4"><button type="button" class="btn btn-danger rounded-pill removeRow">remove metadata</button></div>
            </div>
            </div>

            <div id="newRow"></div>
            <br>
            <div class="row">
                <div class="text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-info addRow">add metadata</button>
            </div>


        </form><!-- End Horizontal Form -->

    </div>
</div>

<script type="text/javascript">
    // add row
    $(".addRow").click(function () {
        var html = '';
        html += '';
        html += `<div class="row" id="inputFormRow"><div class="col-md-4">
                <div class="form-floating">
                    <input type="text" name="metadata_name[]" class="form-control" placeholder="name">
                    <label>name</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input type="text" name="metadata_content[]" class="form-control" placeholder="content">
                    <label>content</label>
                </div>
            </div>
            <div class="col-md-4"><button type="button" class="btn btn-danger rounded-pill removeRow">remove metadata</button></div>
</div>`;


        $('#newRow').append(html);
    });

    // remove row
    $(document).on('click', '.removeRow', function () {
        $(this).closest('#inputFormRow').remove();
    });
</script>
