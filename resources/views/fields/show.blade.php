@extends('layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                @include('fields.search')
                <div class="card">
                    <div class="">
                        <h5 class="card-title">USer Table</h5>
                        <div class="table-responsive">
                            @unless(count($array) == 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <th scope="col">#</th>

                                    @foreach($list_config  as $key=>$field)
                                        <th @if(isset($field['row_width'])) style="width:{{$field['row_width']}}%" @endif>
                                            {{isset($field['row_name'])? $field['row_name']:$key}}
                                            @if(isset($field['sort']))
                                                @php
                                                    if($field['sort'] == 'ASC')
                                                        $sort_order = 'DESC';
                                                    elseif($field['sort'] == 'DESC')
                                                        $sort_order = 'ASC';
                                                @endphp
                                            <a href="@php echo \Request::fullUrlWithQuery([$key.'[sort]'=>$sort_order]);
                                                @endphp">sort</a>

                                            @endif
                                        </th>

                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($array as $item)
                                    <tr>
                                        <td></td>
                                        @foreach($list_fields as $field)
                                           <td>{{$item[$field]}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            @else
                                no list
                            @endunless
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(count($array))
        {{ $array->withQueryString()->links() }}
        {{--@if(\Request::query())
            {{ $array->withQueryString()->links() }}
        @else
            {{ $array->links() }}
        @endif--}}
    @endif

    <script>
        $(document).ready(function(){
            $("#search_form_field").on("submit", function(event){
                event.preventDefault();
                var formValues= $(this).serialize();

                $.get("@php echo \Request::fullUrl() @endphp", formValues, function(data){
                    // Display the returned data in browser
                    $('#page').html(data)
                    console.log(data);
                });
            });
        });
    </script>


@endsection('content')



