<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Collection;
use function PHPUnit\Framework\exactly;

class FieldController extends Controller
{
    //
    public function edit()
    {
        $all_fields = Field::all();
        return view('fields.index' , compact(['all_fields']));

    }

    public function update(Request $request)
    {
       // die(json_encode($request->all()));
        $data=[];
        $id=[];
        $fields = Field::all();
        $all_request = $request->all() ;

      //  die(json_encode($all_request));
        for ($i=1 ; $i<= count($all_request['id']);$i++)
        {
           // ['row_name'][$i]);
            //die(json_encode($all_request[0]));
            $data= array('row_name'=>$all_request['row_name'][$i] ,'row_width'=>$all_request['row_width'][$i] ,
                'show_in_list'=>isset($all_request['show_list'][$i] )? 1:0 ,'show_in_search'=>isset($all_request['show_search'][$i])? 1:0 ,
                    'field_name'=>$all_request['field_name'][$i] ,'id'=>$all_request['id'][$i] ,'sort'=>$all_request['sort'][$i] ,
                );
            $fields[$i-1]->update($data);
        }

    }
    public function store()
    {
        $array = (json_decode(json_decode(\DB::table('mock_api')->get())[0]->json)[0]);
        $row_id = 0;
        foreach ($array as $key => $val) {
            $row_id++;
            $form_fields[] = array('field_name'=>$key , 'row_name'=>"row $row_id");
        }
        Field::insert($form_fields);

       // var_dump($all_fields);
    }

    public function show(Request $request)
    {
        $table_field_name =  Schema::getColumnListing('users');
        $control_fields = ['row_name','row_width','field_type','search','sort','list'];
        $all_request = $request->all();
        $request_keys =array_keys($all_request);

        //find fields must show in frontend list
        $url_fields=[];

        foreach ($all_request as $k=>$request_key)
        {
            if(isset($all_request[$k]['list']))
                $url_fields[] = $k;

        }

        $list_fields = array_intersect($url_fields,$table_field_name);
        if(count($list_fields) > 0) {

            $array = User::select($list_fields);

            $array->where(function($array) use ($all_request) {
                foreach($all_request as $k=>$request_key){

                    if (str_contains( $k ,'search_')) {
                        $q = str_replace('search_', '', $k);
                        if(isset($request_key))
                        {
                            $array->Where($q, 'LIKE', "%$request_key%");

                        }
                    }
                }
            });

            $list_config = [];
            $search_list = [];
            $item_has_type=[];

            foreach ($list_fields as $list_field) {
                if (isset($all_request[$list_field]))
                    $column_config_url = array_keys($all_request[$list_field]);
                else
                    $column_config_url = [];
                if (isset($column_config_url) && $column_config_url) {
            foreach ($column_config_url as $item) {

                        if (in_array($item, $control_fields)) {
                            if ($item == 'search') {
                                $search_list[] = $list_field;
                            }
                            if ($item == 'field_type') {


                                $item_has_type[$list_field] = $all_request[$list_field][$item];
                            }
                            if ((in_array('sort', $column_config_url))) {
                                $array = $array->orderby($list_field, $all_request[$list_field]['sort']);
                            }
                            if(!in_array('row_name' , $column_config_url))
                                $list_config[$list_field]['row_name'] = $list_field;



                            $list_config[$list_field][$item] = ($all_request[$list_field][$item]);
                        }
                    }
                }
            }

            $array = $array->withCasts(
                $item_has_type
           );



            if (in_array('count_per_page', $request_keys)) {
                $count_per_page = $all_request['count_per_page'];
            } else
                $count_per_page = 2;

            $array = $array->paginate($count_per_page);

            $page_config = [];
            if (isset($all_request['metadata']) && isset($all_request['metadata']['name']) && isset($all_request['metadata']['content'])) {
                foreach ($all_request['metadata']['name'] as $k1 => $metadata_name) {

                    foreach ($all_request['metadata']['content'] as $k2 => $metadata_content) {

                        if ($k1 == $k2)
                            $page_config['metadata'][] =
                                [
                                    'name' => $metadata_name,
                                    'content' => $metadata_content
                                ];
                        else
                            continue;

                    }
                }
            }
        }
        else
        {
            $array = [];
            $list_config = [];
            $page_config = '';
            $search_list = [];
        }





            return view('fields.show', compact('list_fields', 'array',
                'list_config', 'page_config', 'search_list'));

    }





}
