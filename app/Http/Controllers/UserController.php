<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class UserController extends Controller
{
    //

    public function webservice(Request $request)
    {
        $list_fields = ['name', 'email','gender'];

        $list_config = [
            "name"=>[
                "row_width"=>10,
                "list"=>1,
                "search"=>1,
                "row_name"=>"user_name",
            ],
            "email"=>
            [
                "row_width"=>30,
                "list"=>1,
                "row_name"=>"user_email"
            ],
            "gender"=>
            [
                "row_name"=>"user_gender",
                "list"=>1,
                "search"=>1,
                "sort"=>'ASC'
            ]
        ];



        $search_list=['name','gender'];

        $page_config = [
            'count_per_page'=>6,
            'metadata'=>[
            [
                'name'=>'metadata1',
                'content'=>'content1'
            ],
            [
                'name'=>'metadata2',
                'content'=>'content2'
            ]
                ]
        ];
        $item_has_type = ['name'=>'string'];
        $casting = User::select($list_fields)->withCasts(
            $item_has_type
        );

        $all_request = $request->all();

        $casting->where(function($array) use ($all_request) {
            foreach($all_request as $k=>$request_key){

                if (str_contains( $k ,'search_')) {
                    $q = str_replace('search_', '', $k);
                    if(isset($request_key))
                    {
                        $array->Where($q, 'LIKE', "%$request_key%");

                    }
                }
            }
        });
        foreach ($list_fields as $list_field) {

            if (in_array('sort', array_keys($list_config[$list_field]))) {
                if(isset($request->all()[$list_field]['sort']))
                {
                    if ($request->all()[$list_field]['sort'] == 'DESC')
                        $list_config[$list_field]['sort'] = 'ASC';
                    else
                        $list_config[$list_field]['sort'] = 'DESC';
                }

                $array = $casting->orderby($list_field, $list_config[$list_field]['sort']);
            }
        }

        $array = $array->paginate($page_config['count_per_page']);
        return view('users.show',compact('list_fields','array','list_config','page_config','search_list'));







    }
}
