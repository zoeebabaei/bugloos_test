<?php

namespace App\Http\Controllers;

use App\Models\Metadata;
use Illuminate\Http\Request;

class MetadataController extends Controller
{
    //
    public function store(Request $request)
    {
        $all_request = $request->all();
        for ($i=0;$i<count($request['metadata_name']);$i++)
        {
            $form_fields[] = array('name'=>$request['metadata_name'][$i] , 'content'=>$request['metadata_content'][$i]);
        }
        Metadata::insert($form_fields);
    }
}
